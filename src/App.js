import React from 'react';
import './App.css';
import InputField from './Components/InputField';
import List from './Components/List';

export default class App extends React.Component {
  constructor(props) {
    super(props)
  
    this.state = {
       results: [],
       repos: []
    }
  }
  handleResults = (data) => {
    this.setState({ results: data })
  } 
  handleRepos = (repos) => {
    this.setState({ repos : repos})
  }
  
  render() {

    return (
      <div className="App">
        <InputField rezultat={this.handleResults} repos={this.handleRepos}/>
        <List data={this.state.results} repos={this.state.repos}/>
      </div>
    );
  }
}


