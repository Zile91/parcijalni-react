import React, { Component } from 'react';
import PropTypes from 'prop-types';

export default class InputField extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
             username: ""
        }
    }
    inputChange = (e) => {
        this.setState({ username:e.target.value })
    }

    searchHandler = (e) => {
        e.preventDefault();

        fetch('https://api.github.com/users/' + this.state.username)
        .then(response => response.json())
        .then(dataObject => {
            this.props.rezultat(dataObject);

            
        });

        fetch('https://api.github.com/users/' + this.state.username + '/repos')
        .then(response => response.json())
        .then(dataObject => {
            this.props.repos(dataObject);

            this.setState({ username:""});
        });

    }
    
    render() {
        return (
            <div>
                <form onSubmit={this.searchHandler}>
                    <input required type="text" value={this.state.username} onChange={this.inputChange}/>
                    <input type="submit" value="GO!"/>
                </form>
            </div>
        )
    }
}

InputField.propTypes = {
    rezultat: PropTypes.func,
    repos: PropTypes.func
}