import React from 'react';
import PropTypes from 'prop-types';



export default function List({ data, repos}) {
    return (
        <div>
            <img alt={data.name} src={data.avatar_url} />
            <h1>{data.name}</h1>
            <p>BIO: {data.bio}</p>
            <p>Location: {data.location}</p>

        <p>REPOSITORIES:</p>
        <ul>
        {repos.map((item) => {
            return (<li key={item.id}>{item.name}</li>);
        })}
        </ul>
        </div>
        
    );
}
List.propTypes = {
    data: PropTypes.func,
    repos: PropTypes.array
}
